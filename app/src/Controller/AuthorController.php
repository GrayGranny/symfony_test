<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

use App\Repository\AuthorRepository;
use App\Entity\Author;
use App\Form\AuthorType;

class AuthorController extends AbstractController
{
    /**
     * Lists all Author entities.
     * @Route("/author/{page}", name="author_list", requirements={"page"="\d+"})
     */
    public function index(AuthorRepository $authorRepository, int $page = 1): Response {

        $paginator = $authorRepository->findAll($page);
        $maxPages = ceil($paginator->count() / $authorRepository::PAGE_SIZE);
        
        return $this->render(
            'author/index.html.twig',
            [
                'paginator' => $paginator,
                'maxPages' => $maxPages,
                'thisPage' => $page,
            ]
        );
    }

    /**
     * Finds and displays an Author entity.
     * @Route("/author/show/{id}", name="show_author")
     */
    public function show(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $author = $entityManager->getRepository(Author::class)->find($id);

        if (!$author) {
            throw $this->createNotFoundException(
                'Автор с id '. $id . ' не найден' 
            );
        }

        return $this->renderForm('author/show.html.twig', [
            'author' => $author,
            'books' => $author->getBooks(),
        ]);
    }

    /**
     * Creates a new existing Author entity.
     * @Route("/author/new", name="new_author")
     */
    public function new(Request $request, AuthorRepository $authorRepository): Response
    {
        $author = new Author();

        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $author = $form->getData();
            $authorRepository->add($author);

            $this->addFlash(
                'success',
                'Автор успешно добавлен'
            );
            return $this->redirectToRoute('author_list');
        }

        return $this->renderForm('author/new.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * Displays a form to edit an existing Author entity.
     * @Route("/author/edit/{id}", name="edit_author")
     */
    public function edit(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $author = $entityManager->getRepository(Author::class)->find($id);

        if (!$author) {
            throw $this->createNotFoundException(
                'Автор с id '. $id . ' не найден' 
            );
        }
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $author = $form->getData();
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Автор успешно изменён'
            );

            return $this->redirectToRoute('author_list');
        }

        return $this->renderForm('author/edit.html.twig', [
            'form' => $form,
            'author' => $author,
        ]);
    }

    /**
     * Deletes an Author entity.
     * @Route("/author/delete/{id}", name="delete_author", methods={"POST"})
     */
    public function delete(Request $request, Author $author, ManagerRegistry $doctrine): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('author_list');
        }

        $entityManager = $doctrine->getManager();
        $entityManager->remove($author);
        $entityManager->flush();

        $this->addFlash(
            'success',
            'Автор с был успешно удалён!'
        );

        return $this->redirectToRoute('author_list');
    }
}
