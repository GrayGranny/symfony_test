<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Repository\BookRepository;
use App\Entity\Book;
use App\Form\BookType;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;

class BookController extends AbstractController
{
    /**
     * @Route("/book/{page}", name="book_list", requirements={"page"="\d+"})
     */
    public function index(BookRepository $bookRepository, int $page = 1): Response {

        $paginator = $bookRepository->findAll($page);
        $maxPages = ceil($paginator->count() / $bookRepository::PAGE_SIZE);
        
        return $this->render(
            'book/index.html.twig',
            [
                'paginator' => $paginator,
                'maxPages' => $maxPages,
                'thisPage' => $page,
            ]
        );
    }

    /**
     * @Route("/book/new", name="new_book")
     */
    public function new(Request $request, BookRepository $bookRepository, ManagerRegistry $doctrine, FileUploader $fileUploader): Response
    {
        $entityManager = $doctrine->getManager();
        $authorExists = $entityManager->getRepository(\App\Entity\Author::class)->exists();

        if(!$authorExists) {

            $this->addFlash(
                'warning',
                'Чтобы добавить книгу - добавьте хотя бы 1 автора'
            );

            return $this->redirectToRoute('book_list');
        }

        $book = new Book();

        if ($author_id = $request->query->get('author')) {
            $author = $entityManager->getRepository(\App\Entity\Author::class)->find($author_id);
            if ($author) {
                $book->addAuthor($author);
            }
        }

        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $book = $form->getData();
            $coverFile = $form->get('cover')->getData();
            if ($coverFile) {
                $coverFileName = $fileUploader->upload($coverFile);

                if (!$coverFileName) {
                    $this->addFlash(
                        'danger',
                        'Не удалось загрузить файл обложки. Попробуйте загрузить другой файл или обратитесь в службу поддержки'
                    );

                    return $this->renderForm('book/new.html.twig', [
                        'form' => $form,
                        'form_title' => 'Добавление книги'
                    ]);
                }

                $book->setCoverFilename($coverFileName);
            }

            $bookRepository->add($book);

            $this->addFlash(
                'success',
                'Книга успешно добавлена'
            );

            return $this->redirectToRoute('book_list');
        }

        return $this->renderForm('book/new.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/book/edit/{id}", name="edit_book")
     */
    public function edit(int $id, Request $request, ManagerRegistry $doctrine, FileUploader $fileUploader): Response
    {
        $entityManager = $doctrine->getManager();

        $book = $entityManager->getRepository(Book::class)->find($id);

        if (!$book) {
            throw $this->createNotFoundException(
                'Книга с id '. $id . ' не найдена' 
            );
        }

        $oldCoverFile = $book->getCoverFilename();

        if ($oldCoverFile) {
            $fullPath = $fileUploader->getTargetDirectory().'/'.$oldCoverFile;
            if (file_exists($fullPath)) {
                $book->setCoverFilename(
                    new File($fullPath)
                );
            }
        }

        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $book = $form->getData();
            $coverFile = $form->get('cover')->getData();
            if ($coverFile) {
                $coverFileName = $fileUploader->upload($coverFile, $oldCoverFile);
                $book->setCoverFilename($coverFileName);

                if (!$coverFileName) {
                    $this->addFlash(
                        'danger',
                        'Не удалось загрузить файл обложки. Попробуйте загрузить другой файл или обратитесь в службу поддержки'
                    );

                    return $this->renderForm('book/edit.html.twig', [
                        'form' => $form,
                        'form_title' => 'Добавление книги'
                    ]);
                }

            } else if ($oldCoverFile) {
                $book->setCoverFilename($oldCoverFile);
            }
            
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Книга успешно изменена'
            );

            return $this->redirectToRoute('book_list');
        }

        return $this->renderForm('book/edit.html.twig', [
            'form' => $form,
            'imagePath' => $oldCoverFile,
        ]);
    }

    /**
     * Deletes a Book entity.
     * @Route("/book/delete/{id}", name="delete_book", methods={"POST"})
     */
    public function delete(Request $request, Book $book, ManagerRegistry $doctrine): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('book_list');
        }

        $entityManager = $doctrine->getManager();
        $entityManager->remove($book);
        $entityManager->flush();

        $this->addFlash(
            'success',
            'Книга была успешно удалена!'
        );

        return $this->redirectToRoute('book_list');
    }
}
