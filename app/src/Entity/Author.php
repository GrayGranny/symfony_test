<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 */
class Author
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $patronymic;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, mappedBy="authors")
     */
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }


    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new Assert\NotNull([
            'message' => 'Укажите имя автора'
        ]));
        $metadata->addPropertyConstraint('name', new Assert\Length([
            'max'        => 100,
            'maxMessage' => 'Имя не должно быть длиннее {{ limit }} символов',
        ]));
        
        $metadata->addPropertyConstraint('surname', new Assert\NotNull([
            'message' => 'Укажите фамилию автора'
        ]));
        $metadata->addPropertyConstraint('surname', new Assert\Length([
            'max'        => 100,
            'maxMessage' => 'Фамилия не должна быть длиннее {{ limit }} символов',
        ]));

        $metadata->addPropertyConstraint('patronymic', new Assert\Length([
            'max'        => 100,
            'maxMessage' => 'Отчество не должно быть длиннее {{ limit }} символов',
        ]));

        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['surname', 'name', 'patronymic'],
            'errorPath' => 'surname',
            'ignoreNull' => false,
            'message' => 'Автор с таким ФИО уже добавлен.',
        ]));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function setPatronymic(?string $patronymic): self
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return Collection<int, Book>
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->addAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            $book->removeAuthor($this);
        }

        return $this;
    }

    public function getFullName(): ?string
    {
        return sprintf('%s %s %s', $this->getSurname(), $this->getName(), $this->getPatronymic());
    }
}
