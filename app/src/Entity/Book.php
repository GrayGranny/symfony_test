<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=17)
     */
    private $isbn;

    /**
     * @ORM\Column(type="integer")
     */
    private $pages;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="books")
     */
    private $authors;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $coverFilename;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('title', new Assert\NotNull([
            'message' => 'Укажите название книги'
        ]));
        
        $metadata->addPropertyConstraint('isbn', new Assert\NotNull([
            'message' => 'Укажите ISBN'
        ]));

        $metadata->addPropertyConstraint('year', new Assert\NotNull([
            'message' => 'Укажите год издания'
        ]));

        $metadata->addPropertyConstraint('year', new Assert\Length([
            'max'        => 4,
            'maxMessage' => 'Фамилия не должна быть длиннее {{ limit }} символов',
        ]));

        $metadata->addPropertyConstraint('year', new Assert\LessThanOrEqual([
            'value' => date('Y'),
            'message' => 'Год издания не может быть больше текущего',
        ]));

        $metadata->addPropertyConstraint('pages', new Assert\NotNull([
            'message' => 'Укажите количество страниц'
        ]));

        $metadata->addPropertyConstraint('isbn', new Assert\Isbn([
            'message' => 'Невалидный ISBN.',
        ]));

        $metadata->addPropertyConstraint('pages', new Assert\Type([
            'type' => 'integer',
            'message' => 'Количество страниц должно быть числом.',
        ]));

        $metadata->addPropertyConstraint('pages', new Assert\GreaterThan([
            'value' => 0,
            'message' => 'Количество страниц должно быть больше {{ compared_value }}',
        ]));

        $metadata->addPropertyConstraint('pages', new Assert\LessThan([
            'value' => 1000000,
            'message' => 'Количество страниц должно быть меньше {{ compared_value }}',
        ]));

        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['isbn', 'title'],
            'message' => 'Книга с таким названием и ISBN уже существует.',
        ]));

        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['year', 'title'],
            'message' => 'Книга с таким названием и годом издания уже существует.',
        ]));

        $metadata->addPropertyConstraint('authors', new Assert\Count([
            'min' => 1,
            'minMessage' => 'Вы должны указать как минимум 1 автора',
        ]));
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getPages(): ?int
    {
        return $this->pages;
    }

    public function setPages(int $pages): self
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * @return Collection<int, Author>
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        $this->authors->removeElement($author);

        return $this;
    }

    public function getCoverFilename(): ?string
    {
        return $this->coverFilename;
    }

    public function setCoverFilename(string $coverFilename): self
    {
        $this->coverFilename = $coverFilename;

        return $this;
    }
}
