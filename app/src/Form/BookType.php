<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Название книги'
            ])
            ->add('year', TextType::class, [
                'label' => 'Год издания',
                'attr' => ['pattern' => '[1-2]{1}[0-9]{3}', 'maxlength' => 4],
                'help' => 'Укажите год, например, 1995',
            ])
            ->add('isbn', TextType::class, [
                'label' => 'ISBN',
                'help' => 'Введите только числовое значение. Например: 2-266-11156-6',
            ])
            ->add('pages', IntegerType::class, [
                'label' => 'Количество страниц'
            ])
            ->add('pages', IntegerType::class, [
                'label' => 'Количество страниц'
            ])
            ->add('authors', EntityType::class, [
                'label' => 'Автор(ы)',
                'class' => 'App\Entity\Author',
                'query_builder' => function (\App\Repository\AuthorRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->orderBy('a.surname', 'ASC');
                },
                'choice_label' => function(?\App\Entity\Author $author) {
                    return $author ? $author->getFullName() : '';
                },
                'multiple' => true,
                'expanded' => false,
            ])
             ->add('cover', FileType::class, [
                'label' => 'Обложка книги (png или jpeg файл)',
                'mapped' => false,
                'required' => false,
                'attr' => ['accept'=> 'image/jpeg,image/png,image/jpg', 'placeholder' => 'Выберите файл'],
                'constraints' => [
                    new File([
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => 'Загрузите png или jpeg файл',
                        'notFoundMessage' => 'Файл не найден',
                        'maxSizeMessage' => 'Файл слишком большой ({{ size }} {{ suffix }}). Максимально разрешенный размер файла {{ limit }} {{ suffix }}.'
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
